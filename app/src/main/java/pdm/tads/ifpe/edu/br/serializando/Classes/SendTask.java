package pdm.tads.ifpe.edu.br.serializando.Classes;

import android.net.Uri;
import android.os.AsyncTask;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Pedro Jatoba on 04-Jan-17.
 */
public class SendTask extends AsyncTask<String, Void, String[]> {

    HttpURLConnection urlConnection = null;

    @Override
    protected String[] doInBackground(String ... param) {
        String[] result = new String[10];
        try {
            Uri.Builder builder = new Uri.Builder();
            builder.scheme("http");
            builder.encodedAuthority("192.168.124.38:8085");
            builder.appendPath("SerializandoWS");
            builder.appendPath("usuario");
            builder.appendPath("login");
            URL url = new URL(builder.build().toString());

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
        }catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
