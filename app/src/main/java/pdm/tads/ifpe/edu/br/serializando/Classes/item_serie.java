package pdm.tads.ifpe.edu.br.serializando.Classes;

import android.content.Context;

public class item_serie {

    int ratingBar;


    public int idItemSerie, temporadas;
    Context context;

    public String nomeSerie, descricaoSerieCompleta, descricaoSerieSimples, linkImagem;

    public item_serie() {
    }

    public item_serie(Context context) {
        this.context = context;
    }

    public item_serie(int idItemSerie, String nomeSerie, String descricaoSerieCompleta,
                     String descricaoSerieSimples, String linkImagem, int temporadas) {
        this.idItemSerie = idItemSerie;
        this.nomeSerie = nomeSerie;
        this.descricaoSerieCompleta = descricaoSerieCompleta;
        this.descricaoSerieSimples = descricaoSerieSimples;
        this.linkImagem = linkImagem;
        this.temporadas = temporadas;
    }

    public String getImagem() {
        return linkImagem;
    }

    public String getNomeSerie() {
        return nomeSerie;
    }

    public String getDescricaoSerieComlpeta() {
        return descricaoSerieCompleta;
    }

    public String getDescricaoSerieSimples() {
        return descricaoSerieSimples;
    }

    public int getRatingBar() {
        return ratingBar;
    }

    public int getIdItemSerie() {
        return idItemSerie;
    }

    public int getTemporadas() {
        return temporadas;
    }

    public void setTemporadas(int temporadas) {
        this.temporadas = temporadas;
    }

    public void setIdItemSerie(int idItemSerie) {
        this.idItemSerie = idItemSerie;
    }

    public void setImagem(String linkImagem) {
        this.linkImagem = linkImagem;
    }

    public void setNomeSerie(String nomeSerie) {
        this.nomeSerie = nomeSerie;
    }

    public void setDescricaoSerieCompleta(String descricaoSeriecompleta) {
        this.descricaoSerieCompleta = descricaoSeriecompleta;
    }

    public void setDescricaoSerieSimples(String descricaoSerieSimples) {
        this.descricaoSerieSimples = descricaoSerieSimples;
    }

    public void setRatingBar(int ratingBar) {
        this.ratingBar = ratingBar;
    }

    @Override
    public String toString() {
        return "{\"itemSerie\":{" +
                "\"nome_serie\":\"" + nomeSerie + "\"" +
                ",\"descricao_serie_completa\":\"" + descricaoSerieCompleta + "\"" +
                ",\"descricao_serie_simples\":\"" + descricaoSerieSimples + "\"" +
                ",\"link_imagem\":\"" + linkImagem + "\"" +
                ",\"temporadas\":\"" + temporadas + "\"" +
                "}}";
    }
}
