package pdm.tads.ifpe.edu.br.serializando.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by Pedro Jatoba on 27-Apr-16.
 *
 * Classe responsável por criar as abas de um TabLayout
 */
public class ViewPagerAdapter extends FragmentPagerAdapter{
    /**
     * Declaração de variáveis, o primeiro ArrayList contém os fragmentos que farão parte do TabLayout
     * e o segundo contém os titulos que aparecerão em cada aba em um relacionamento de 1 para 1
     */
    ArrayList<Fragment> fragmentos = new ArrayList<>();
    ArrayList<String> titulos_fragmentos = new ArrayList<>();

    /**
     * Construtor base e obrigatório
     * @param fm
     */
    public ViewPagerAdapter(android.support.v4.app.FragmentManager fm){
        super(fm);
    }

    /**
     * Esse método monta os ArrayLists de fragmentos e de titulos respectivamente
     * @param fragmento
     * @param titulo
     */
    public void addAba(Fragment fragmento, String titulo){
        this.fragmentos.add(fragmento);
        this.titulos_fragmentos.add(titulo);
    }

    /**
     * Retorna um fragmento pela sua posição
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        return fragmentos.get(position);
    }

    /**
     * Conta a quantidade de fragmentos que estão no ArrayList
     * @return
     */
    @Override
    public int getCount() {
        return fragmentos.size();
    }

    /**
     * Retorna o titulo de uma aba dependendo da posição
     * @param position
     * @return
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return titulos_fragmentos.get(position);
    }
}
