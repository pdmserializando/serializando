package pdm.tads.ifpe.edu.br.serializando.Atividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import pdm.tads.ifpe.edu.br.serializando.R;

public class login_tela extends AppCompatActivity {

    /**
     * Método onCreate normal, nele por enquanto só é atrelado uma função de Intent nos botões
     * login e criar para entrar nas activities de tela_principal e login_criacao respectivamente
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_tela);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button login = (Button) findViewById(R.id.button_login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent telaPrincipal = new Intent(login_tela.this, tela_principal.class);
                startActivity(telaPrincipal);
            }
        });

        Button criador = (Button) findViewById(R.id.button_criador);
        criador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginCreation = new Intent(login_tela.this, login_criacao.class);
                startActivity(loginCreation);
            }
        });
    }

    /**
     * Esse método ainda não faz nada e não acho que vá ser usado, não deleto para não causar problemas
      * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
