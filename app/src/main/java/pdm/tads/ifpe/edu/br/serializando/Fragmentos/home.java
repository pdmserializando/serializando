package pdm.tads.ifpe.edu.br.serializando.Fragmentos;

import pdm.tads.ifpe.edu.br.serializando.Adapters.ItemSerieAdapter;
import pdm.tads.ifpe.edu.br.serializando.Atividades.detalhe_serie;
import pdm.tads.ifpe.edu.br.serializando.Classes.RequestTask;
import pdm.tads.ifpe.edu.br.serializando.Classes.item_serie;
import pdm.tads.ifpe.edu.br.serializando.Interfaces.RequestListener;
import pdm.tads.ifpe.edu.br.serializando.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment implements RequestListener{

    item_serie[] lista_itens = new item_serie[5];

    ListView listView;

    public Home() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        ViewGroup  rootView = (ViewGroup) inflater.inflate(R.layout.layout_item_serie, container, false);
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        try {

            inicializadorItens(new RequestTask(this).execute().get());

        }catch (Exception e){
            System.out.println(e);
        }
        listView = (ListView) view.findViewById(R.id.list_view_lista_series);
        listView.setAdapter(new ItemSerieAdapter(getContext(), R.layout.layout_item_serie, lista_itens));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                Toast.makeText(parent.getContext(),lista_itens[position].getImagem(), Toast.LENGTH_SHORT).show();
                Intent descricao_serie = new Intent(getContext(), detalhe_serie.class);
                startActivity(descricao_serie);
            }
        });
        return view;
    }

    public void inicializadorItens(String[] listaItensStr) throws JSONException{
        final String OWM_ITEM = "itemSerie";
        final String OWM_descricao_serie_simples = "descricao_serie_simples";
        final String OWM_descricao_serie_completa = "descricao_serie_completa";
        final String OWM_link_imagem = "link_imagem";
        final String OWM_nome_serie = "nome_serie";
        final String OWM_temporadas = "temporadas";

        for (int i = 0; i < listaItensStr.length; i++){

            JSONObject jsonAtual = new JSONObject(listaItensStr[i]);
            JSONObject jsonObject = jsonAtual.getJSONObject(OWM_ITEM);


            item_serie item_serie = new item_serie();
            item_serie.nomeSerie = jsonObject.getString(OWM_nome_serie);
            item_serie.descricaoSerieCompleta = jsonObject.getString(OWM_descricao_serie_completa);
            item_serie.descricaoSerieSimples = jsonObject.getString(OWM_descricao_serie_simples);
            item_serie.linkImagem = jsonObject.getString(OWM_link_imagem);
            item_serie.temporadas = jsonObject.getInt(OWM_temporadas);

            lista_itens[i] = item_serie;
        }

    }

    @Override
    public void showSeries(String[] series) {
        if (series == null) {
            Toast toast = Toast.makeText(this.getContext(), "Sem series para mostrar", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            ArrayList<CharSequence> data = new ArrayList<CharSequence>(Arrays.asList(series));
        }
    }
}
