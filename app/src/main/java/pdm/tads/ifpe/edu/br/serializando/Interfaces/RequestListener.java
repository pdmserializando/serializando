package pdm.tads.ifpe.edu.br.serializando.Interfaces;

/**
 * Created by Pedro Jatoba on 31-May-16.
 */
public interface RequestListener {
    public void showSeries(String [] series);
}
