package pdm.tads.ifpe.edu.br.serializando.Classes;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import pdm.tads.ifpe.edu.br.serializando.Interfaces.RequestListener;
import pdm.tads.ifpe.edu.br.serializando.Parsers.SerieParser;

/**
 * Created by Pedro Jatoba on 31-May-16.
 */
public class RequestTask extends AsyncTask <String, Void, String[]> {

    private final String LOG_TAG = RequestTask.class.getSimpleName();
    private String [] series = null;
    private RequestListener listener = null;

    public RequestTask(RequestListener listener){
        this.listener = listener;
    }


    @Override
    protected String[] doInBackground(String... params) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String seriesJson = null;

        try {
            Uri.Builder builder = new Uri.Builder();
            builder.scheme("http");
            builder.encodedAuthority("192.168.124.38:8085");
            builder.appendPath("SerializandoWS");
            builder.appendPath("itemSerie");
            builder.appendPath("listarTodasSeries");
            URL url = new URL(builder.build().toString());

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            System.out.println(urlConnection.getResponseCode());
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if(inputStream == null){
                seriesJson = null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while((line = reader.readLine())!= null){
                buffer.append(line + "\n");
                System.out.println(buffer);
            }
            if(buffer.length() == 0){
                seriesJson = null;
            } else{
                seriesJson = buffer.toString();
            }
            series = SerieParser.getDataFromJson(seriesJson);
        }catch (IOException e){
            Log.e(LOG_TAG, "Error ", e);
        }catch (JSONException e){
            Log.e(LOG_TAG, "JSON Error ", e);
        }finally{
            if(urlConnection != null) urlConnection.disconnect();
            if(reader != null){
                try {
                    reader.close();
                }catch (final IOException e){
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }
        return series;
    }

    @Override
    protected void onPostExecute(String[] resultStrs){
        listener.showSeries(resultStrs);
    }
}
