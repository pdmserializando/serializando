package pdm.tads.ifpe.edu.br.serializando.Adapters;

import pdm.tads.ifpe.edu.br.serializando.Classes.item_serie;
import pdm.tads.ifpe.edu.br.serializando.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Pedro Jatoba on 02-May-16.
 */
public class ItemSerieAdapter extends ArrayAdapter<item_serie> {

    ImageView imagem;
    TextView nomeSerie;
    TextView descricaoSerie;
    RatingBar ratingBar;
    item_serie[] lista_itens;

    public ItemSerieAdapter(Context context, int resource, item_serie[] lista_itens) {
        super(context, resource, lista_itens);
        this.lista_itens = lista_itens;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        View listItem = null;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            listItem = inflater.inflate(R.layout.layout_item_serie, null, true);
        } else {
            listItem = view;
        }

        imagem = (ImageView) listItem.findViewById(R.id.image_view_imagem_serie);
        nomeSerie = (TextView) listItem.findViewById(R.id.text_view_nome_serie);
        descricaoSerie = (TextView) listItem.findViewById(R.id.text_view_descricao_serie);
        ratingBar = (RatingBar) listItem.findViewById(R.id.rating_bar_rating_serie);


        Picasso.with(listItem.getContext()).load(lista_itens[position].getImagem()).into(imagem);
        nomeSerie.setText(lista_itens[position].getNomeSerie());
        descricaoSerie.setText(lista_itens[position].getDescricaoSerieSimples());
        return listItem;
    }
}
