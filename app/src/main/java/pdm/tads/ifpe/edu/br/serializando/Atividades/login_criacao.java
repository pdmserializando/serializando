package pdm.tads.ifpe.edu.br.serializando.Atividades;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pdm.tads.ifpe.edu.br.serializando.R;

public class login_criacao extends AppCompatActivity {

    /**
     * Declarações de todos os editText que estão no layout.
     */
    EditText edit_text_name;
    EditText edit_text_nickname;
    EditText edit_text_email;
    EditText edit_text_password;
    EditText edit_text_confirmedPassword;
    EditText edit_text_address;

    /**
     * Declarações das variáveis que ficarão atreladas aos editText.
     */
    protected String name;
    protected String nickname;
    protected String email;
    protected String password;
    protected String confirmedPassword;
    protected String address;

    /**
     * Método onCreate que inicializa os EditText's e atrela a cada um deles um TextWatcher
     * para que as variáveis locais possam ser modificadas em tempo de execução.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_criacao);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        edit_text_name = (EditText) findViewById(R.id.edit_text_name);
        edit_text_nickname = (EditText) findViewById(R.id.edit_text_nickname);
        edit_text_email = (EditText) findViewById(R.id.edit_text_email);
        edit_text_password = (EditText) findViewById(R.id.edit_text_password);
        edit_text_confirmedPassword = (EditText) findViewById(R.id.edit_text_confirmed_password);
        edit_text_address = (EditText) findViewById(R.id.edit_text_address);

        edit_text_name.addTextChangedListener(edit_text_name_listener);
        edit_text_nickname.addTextChangedListener(edit_text_nickname_listener);
        edit_text_email.addTextChangedListener(edit_text_email_listener);
        edit_text_password.addTextChangedListener(edit_text_password_listener);
        edit_text_confirmedPassword.addTextChangedListener(edit_text_confirmedPassword_listener);
        edit_text_address.addTextChangedListener(edit_text_address_listener);

        Button sent = (Button) findViewById(R.id.button_sent_data);
        sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser(name, nickname, email, password, confirmedPassword, address);
            }
        });
    }

    /**
     * Declaração do TextWacther do EditText nome, o TextWatcher serve como um "ouvidor" de modificções
     * em cima do EditText da activity, sempre que o EditText é atualizado o valor a variável local que guarda
     * seu valor é atualizada também.
     */
    private TextWatcher edit_text_name_listener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            name = s.toString();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * Declaração do TextWacther do EditText nickname, o TextWatcher serve como um "ouvidor" de modificções
     * em cima do EditText da activity, sempre que o EditText é atualizado o valor a variável local que guarda
     * seu valor é atualizada também.
     */
    private TextWatcher edit_text_nickname_listener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            nickname = s.toString();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * Declaração do TextWacther do EditText email, o TextWatcher serve como um "ouvidor" de modificções
     * em cima do EditText da activity, sempre que o EditText é atualizado o valor a variável local que guarda
     * seu valor é atualizada também.
     */
    private TextWatcher edit_text_email_listener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            email = s.toString();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * Declaração do TextWacther do EditText senha, o TextWatcher serve como um "ouvidor" de modificções
     * em cima do EditText da activity, sempre que o EditText é atualizado o valor a variável local que guarda
     * seu valor é atualizada também.
     */
    private TextWatcher edit_text_password_listener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            password = s.toString();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * Declaração do TextWacther do EditText confirmacaoSenha, o TextWatcher serve como um "ouvidor" de modificções
     * em cima do EditText da activity, sempre que o EditText é atualizado o valor a variável local que guarda
     * seu valor é atualizada também.
     */
    private TextWatcher edit_text_confirmedPassword_listener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            confirmedPassword = s.toString();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * Declaração do TextWacther do EditText endereco, o TextWatcher serve como um "ouvidor" de modificções
     * em cima do EditText da activity, sempre que o EditText é atualizado o valor a variável local que guarda
     * seu valor é atualizada também.
     */
    private TextWatcher edit_text_address_listener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            address = s.toString();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * O método createUser deverá construir o objeto que será utilizado para salvar o novo usuário no banco
     * de dados, por enquanto ele apenas mostra um pop-up com os valores das variáveis que representas os
     * dados do usuário.
     * @param name
     * @param nickname
     * @param email
     * @param password
     * @param confirmedPassword
     * @param address
     */
    protected void createUser(String name, String nickname, String email, String password,
                              String confirmedPassword, String address) {

        Toast.makeText(this, name, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, nickname, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, email, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, password, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, confirmedPassword, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, address, Toast.LENGTH_SHORT).show();
    }
}
