package pdm.tads.ifpe.edu.br.serializando.Atividades;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import pdm.tads.ifpe.edu.br.serializando.Adapters.ViewPagerAdapter;
import pdm.tads.ifpe.edu.br.serializando.Fragmentos.*;
import pdm.tads.ifpe.edu.br.serializando.R;

public class tela_principal extends AppCompatActivity {
    /**
     * Declarção de variáveis, variável toolbar: Relaciona a aba principal da activity; tabLayout:
     * relaciona completamente o layout Tab; viewPager: objeto que é respoensável por apresentar todas
     * as abas do tabLayout; viewPagerAdapter: responsável por construir cada aba do tabLayout e relacionar ela com
     * um titulo.
     */
    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;

    /**
     * Método onCreate que por enquanto tem como função estruturar o tabLayout, primeiro criando
     * uma toolbar básica, depois inicializando o layout Tab, seguido pelo viewPager e o viewPagerAdapter
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        toolbar = (Toolbar) findViewById(R.id.tela_principal_toolbar);
        setSupportActionBar(toolbar);

        tabLayout = (TabLayout) findViewById(R.id.tala_principal_tabLayout);

        viewPager = (ViewPager) findViewById(R.id.tela_principal_viewPager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addAba(new Home(), "Home");
        viewPagerAdapter.addAba(new favoritos(), "Serializadas");

        //Adicionando o viewPagerAdapter no viewPage
        viewPager.setAdapter(viewPagerAdapter);

        //Relacionando esse tabLayout com o o viewPager já com o viewPagerAdapter setado.
        tabLayout.setupWithViewPager(viewPager);


    }

    /**
     * Método pra criar o icone de settings
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

}
