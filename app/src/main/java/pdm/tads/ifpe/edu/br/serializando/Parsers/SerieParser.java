package pdm.tads.ifpe.edu.br.serializando.Parsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import pdm.tads.ifpe.edu.br.serializando.Classes.item_serie;

/**
 * Created by Pedro Jatoba on 31-May-16.
 */
public class SerieParser {

    public static String[] getDataFromJson(String seriesJsonSTR) throws JSONException {
        final String OWM_ITEM = "itemSerie";
        final String OWM_descricao_serie_simples = "descricao_serie_simples";
        final String OWM_descricao_serie_completa = "descricao_serie_completa";
        final String OWM_id_item_serie = "id_item_serie";
        final String OWM_link_imagem = "link_imagem";
        final String OWM_nome_serie = "nome_serie";
        final String OWM_temporadas = "temporadas";

        JSONObject seriesJson = new JSONObject(seriesJsonSTR);
        JSONArray listaSeries = seriesJson.getJSONArray(OWM_ITEM);
        String[] resultStrs = new String[5];

        for(int i = 0; i<5; i++) {
            JSONObject jsonAtual = listaSeries.getJSONObject(i);
            item_serie item_serie = new item_serie();
            item_serie.idItemSerie = jsonAtual.getInt(OWM_id_item_serie);
            item_serie.nomeSerie = jsonAtual.getString(OWM_nome_serie);
            item_serie.descricaoSerieCompleta = jsonAtual.getString(OWM_descricao_serie_completa);
            item_serie.descricaoSerieSimples = jsonAtual.getString(OWM_descricao_serie_simples);
            item_serie.linkImagem = jsonAtual.getString(OWM_link_imagem);
            item_serie.temporadas = jsonAtual.getInt(OWM_temporadas);
            String item = item_serie.toString();

            resultStrs[i] = item;
        }

        return resultStrs;
    }
}